package fr.univparis8.iut.userservice.service.mapper;

import fr.univparis8.iut.userservice.domain.ChatMessage;
import fr.univparis8.iut.userservice.service.dto.ChatMessagesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity ChatMessage and its DTO ChatMessageDTO.
 */
@Mapper(componentModel = "spring", uses = {ChatDiscussionsMapper.class})
public interface ChatMessagesMapper extends EntityMapper<ChatMessagesDTO, ChatMessage> {

    @Mapping(source = "chatDiscussion.id", target = "chatDiscussionId")
    ChatMessagesDTO toDto(ChatMessage chatMessage);

    @Mapping(source = "chatDiscussionId", target = "chatDiscussion")
    ChatMessage toEntity(ChatMessagesDTO chatMessageDTO);

    default ChatMessage fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setId(id);
        return chatMessage;
    }
}
