package fr.univparis8.iut.userservice.service;

import fr.univparis8.iut.userservice.domain.ChatDiscussion;
import fr.univparis8.iut.userservice.repository.ChatDiscussionRepository;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionDTO;
import fr.univparis8.iut.userservice.service.mapper.ChatDiscussionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ChatDiscussion.
 */
@Service
@Transactional
public class ChatDiscussionService {

    private final Logger log = LoggerFactory.getLogger(ChatDiscussionService.class);

    private final ChatDiscussionRepository chatDiscussionRepository;

    private final ChatDiscussionMapper chatDiscussionMapper;

    public ChatDiscussionService(ChatDiscussionRepository chatDiscussionRepository, ChatDiscussionMapper chatDiscussionMapper) {
        this.chatDiscussionRepository = chatDiscussionRepository;
        this.chatDiscussionMapper = chatDiscussionMapper;
    }

    /**
     * Save a chatDiscussion.
     *
     * @param chatDiscussionDTO the entity to save
     * @return the persisted entity
     */
    public ChatDiscussionDTO save(ChatDiscussionDTO chatDiscussionDTO) {
        log.debug("Request to save ChatDiscussion : {}", chatDiscussionDTO);
        ChatDiscussion chatDiscussion = chatDiscussionMapper.toEntity(chatDiscussionDTO);
        chatDiscussion = chatDiscussionRepository.save(chatDiscussion);
        return chatDiscussionMapper.toDto(chatDiscussion);
    }

    /**
     * Get all the chatDiscussions.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ChatDiscussionDTO> findAll() {
        log.debug("Request to get all ChatDiscussions");
        return chatDiscussionRepository.findAll().stream()
            .map(chatDiscussionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one chatDiscussion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ChatDiscussionDTO> findOne(Long id) {
        log.debug("Request to get ChatDiscussion : {}", id);
        return chatDiscussionRepository.findById(id)
            .map(chatDiscussionMapper::toDto);
    }

    /**
     * Delete the chatDiscussion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ChatDiscussion : {}", id);
        chatDiscussionRepository.deleteById(id);
    }
}
