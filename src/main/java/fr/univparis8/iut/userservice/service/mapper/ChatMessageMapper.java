package fr.univparis8.iut.userservice.service.mapper;

import fr.univparis8.iut.userservice.domain.*;
import fr.univparis8.iut.userservice.service.dto.ChatMessageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ChatMessage and its DTO ChatMessageDTO.
 */
@Mapper(componentModel = "spring", uses = {ChatDiscussionMapper.class})
public interface ChatMessageMapper extends EntityMapper<ChatMessageDTO, ChatMessage> {

    @Mapping(source = "chatDiscussion.id", target = "chatDiscussionId")
    ChatMessageDTO toDto(ChatMessage chatMessage);

    @Mapping(source = "chatDiscussionId", target = "chatDiscussion")
    ChatMessage toEntity(ChatMessageDTO chatMessageDTO);

    default ChatMessage fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setId(id);
        return chatMessage;
    }
}
