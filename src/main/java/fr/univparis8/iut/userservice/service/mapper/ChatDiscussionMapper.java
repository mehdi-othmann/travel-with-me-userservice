package fr.univparis8.iut.userservice.service.mapper;

import fr.univparis8.iut.userservice.domain.*;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ChatDiscussion and its DTO ChatDiscussionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChatDiscussionMapper extends EntityMapper<ChatDiscussionDTO, ChatDiscussion> {


    @Mapping(target = "chatmessages", ignore = true)
    ChatDiscussion toEntity(ChatDiscussionDTO chatDiscussionDTO);

    default ChatDiscussion fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChatDiscussion chatDiscussion = new ChatDiscussion();
        chatDiscussion.setId(id);
        return chatDiscussion;
    }
}
