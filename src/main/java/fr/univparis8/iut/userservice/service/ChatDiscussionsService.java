package fr.univparis8.iut.userservice.service;

import fr.univparis8.iut.userservice.domain.ChatDiscussion;
import fr.univparis8.iut.userservice.repository.ChatDiscussionsRepository;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionDTO;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionsDTO;
import fr.univparis8.iut.userservice.service.mapper.ChatDiscussionsMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ChatDiscussion.
 */
@Service
@Transactional
public class ChatDiscussionsService {

    private final ChatDiscussionsRepository chatDiscussionsRepository;

    private final ChatDiscussionsMapper chatDiscussionsMapper;


    public ChatDiscussionsService(ChatDiscussionsRepository chatDiscussionsRepository,
                                  ChatDiscussionsMapper chatDiscussionsMapper) {
        this.chatDiscussionsRepository = chatDiscussionsRepository;
        this.chatDiscussionsMapper = chatDiscussionsMapper;
    }

    public List<ChatDiscussionsDTO> findAllByTrip(String id) {
        return chatDiscussionsRepository.findAllByTripEquals(id)
            .stream()
            .map(chatDiscussionsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    public Optional<ChatDiscussionsDTO> findOne(Long id) {

        return chatDiscussionsRepository.findById(id)
            .map(chatDiscussionsMapper::toDto);
    }

    public void delete(Long id) {
        chatDiscussionsRepository.deleteById(id);
    }

    public ChatDiscussionsDTO save(ChatDiscussionsDTO chatDiscussionDTO) {
        ChatDiscussion chatDiscussion = chatDiscussionsMapper.toEntity(chatDiscussionDTO);
        chatDiscussion = chatDiscussionsRepository.save(chatDiscussion);
        return chatDiscussionsMapper.toDto(chatDiscussion);
    }
}
