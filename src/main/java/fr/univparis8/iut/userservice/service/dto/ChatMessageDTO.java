package fr.univparis8.iut.userservice.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ChatMessage entity.
 */
public class ChatMessageDTO implements Serializable {

    private Long id;

    private String content;


    private Long chatDiscussionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getChatDiscussionId() {
        return chatDiscussionId;
    }

    public void setChatDiscussionId(Long chatDiscussionId) {
        this.chatDiscussionId = chatDiscussionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChatMessageDTO chatMessageDTO = (ChatMessageDTO) o;
        if (chatMessageDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chatMessageDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChatMessageDTO{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", chatDiscussion=" + getChatDiscussionId() +
            "}";
    }
}
