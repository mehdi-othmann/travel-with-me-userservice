package fr.univparis8.iut.userservice.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ChatDiscussion entity.
 */
public class ChatDiscussionsDTO implements Serializable {

    private Long id;

    private String title;

    private String trip;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrip() {
        return trip;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChatDiscussionsDTO chatDiscussionDTO = (ChatDiscussionsDTO) o;
        if (chatDiscussionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chatDiscussionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChatDiscussionDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", trip='" + getTrip() + "'" +
            "}";
    }
}
