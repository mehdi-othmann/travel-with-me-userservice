package fr.univparis8.iut.userservice.service;

import com.github.javafaker.Faker;
import fr.univparis8.iut.userservice.domain.ChatDiscussion;
import fr.univparis8.iut.userservice.domain.ChatMessage;
import fr.univparis8.iut.userservice.domain.Offer;
import fr.univparis8.iut.userservice.repository.ChatDiscussionsRepository;
import fr.univparis8.iut.userservice.repository.ChatMessagesRepository;
import fr.univparis8.iut.userservice.repository.OfferRepository;
import fr.univparis8.iut.userservice.service.dto.tripservice.TripsDTO;
import fr.univparis8.iut.userservice.web.rest.TripServiceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class GeneratorService {

    private final Logger log = LoggerFactory.getLogger(GeneratorService.class);

    private final TripServiceClient tripServiceClient;

    private final ChatDiscussionsRepository chatDiscussionsRepository;

    private final ChatMessagesRepository chatMessagesRepository;

    private final OfferRepository offerRepository;

    public GeneratorService(TripServiceClient tripServiceClient, ChatDiscussionsRepository chatDiscussionsRepository, ChatMessagesRepository chatMessagesRepository, OfferRepository offerRepository) {
        this.tripServiceClient = tripServiceClient;
        this.chatDiscussionsRepository = chatDiscussionsRepository;
        this.chatMessagesRepository = chatMessagesRepository;
        this.offerRepository = offerRepository;
    }

    public void generateChatDiscussion(int amount) {

        Faker faker = new Faker();
        Random r = new Random();

        List<TripsDTO> tripsDTOS = null;
        try {
            tripsDTOS = tripServiceClient.getAllVoyages();
        } catch (Exception e) {
            log.error("Error with tripService");
        }


        for (int i = 0; i < amount; i++) {
            ChatDiscussion chatDiscussion = new ChatDiscussion();

            if (null != tripsDTOS){
                int randomTrip = r.nextInt(tripsDTOS.size());
                chatDiscussion.setTrip(tripsDTOS.get(randomTrip).getId());
            }

            chatDiscussion.setTitle(faker.lorem().sentence());

            chatDiscussionsRepository.save(chatDiscussion);
            for (int j = 0; j < amount; j++) {
                ChatMessage chatMessage = new ChatMessage();
                chatMessage.setContent(faker.lorem().sentence());
                chatMessage.setChatDiscussion(chatDiscussion);
                chatMessagesRepository.save(chatMessage);
            }



        }
    }

    public void generateOffer(int amount) {
        Faker faker = new Faker();


        for (int j = 0; j < amount; j++) {
            Offer offer = new Offer();
            offer.setName(faker.company().buzzword());
            offer.setPrice(faker.number().randomDouble(2,3,10));
            offerRepository.save(offer);
        }
    }
}
