package fr.univparis8.iut.userservice.service;

import fr.univparis8.iut.userservice.domain.Offer;
import fr.univparis8.iut.userservice.repository.OfferRepository;
import fr.univparis8.iut.userservice.repository.OffersRepository;
import fr.univparis8.iut.userservice.service.dto.OfferDTO;
import fr.univparis8.iut.userservice.service.dto.OffersDTO;
import fr.univparis8.iut.userservice.service.mapper.OfferMapper;
import fr.univparis8.iut.userservice.service.mapper.OffersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Offer.
 */
@Service
@Transactional
public class OffersService {

    private final Logger log = LoggerFactory.getLogger(OffersService.class);

    private final OffersRepository offersRepository;

    private final OffersMapper offersMapper;

    public OffersService(OffersRepository offersRepository, OffersMapper offersMapper) {
        this.offersRepository = offersRepository;
        this.offersMapper = offersMapper;
    }

    @Transactional(readOnly = true)
    public List<OffersDTO> findAll() {
        log.debug("Request to get all Offers");

        return offersRepository.findAll().stream()
            .map(offersMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


}
