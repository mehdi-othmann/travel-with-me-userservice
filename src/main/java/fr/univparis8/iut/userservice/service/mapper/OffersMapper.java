package fr.univparis8.iut.userservice.service.mapper;

import fr.univparis8.iut.userservice.domain.Offer;
import fr.univparis8.iut.userservice.service.dto.OfferDTO;
import fr.univparis8.iut.userservice.service.dto.OffersDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity Offer and its DTO OfferDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OffersMapper extends EntityMapper<OffersDTO, Offer> {



    default Offer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Offer offer = new Offer();
        offer.setId(id);
        return offer;
    }
}
