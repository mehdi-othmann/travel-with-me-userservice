package fr.univparis8.iut.userservice.service.mapper;

import fr.univparis8.iut.userservice.domain.ChatDiscussion;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionDTO;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity ChatDiscussion and its DTO ChatDiscussionDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChatDiscussionsMapper extends EntityMapper<ChatDiscussionsDTO, ChatDiscussion> {


    @Mapping(target = "chatmessages", ignore = true)
    ChatDiscussion toEntity(ChatDiscussionsDTO chatDiscussionsDTO);

    default ChatDiscussion fromId(Long id) {
        if (id == null) {
            return null;
        }
        ChatDiscussion chatDiscussion = new ChatDiscussion();
        chatDiscussion.setId(id);
        return chatDiscussion;
    }
}
