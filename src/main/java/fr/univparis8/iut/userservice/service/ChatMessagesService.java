package fr.univparis8.iut.userservice.service;

import fr.univparis8.iut.userservice.domain.ChatMessage;
import fr.univparis8.iut.userservice.repository.ChatMessageRepository;
import fr.univparis8.iut.userservice.repository.ChatMessagesRepository;
import fr.univparis8.iut.userservice.service.dto.ChatMessageDTO;
import fr.univparis8.iut.userservice.service.dto.ChatMessagesDTO;
import fr.univparis8.iut.userservice.service.mapper.ChatMessageMapper;
import fr.univparis8.iut.userservice.service.mapper.ChatMessagesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ChatMessage.
 */
@Service
@Transactional
public class ChatMessagesService {

    private final ChatMessagesRepository chatMessageRepository;

    private final ChatMessagesMapper chatMessageMapper;

    public ChatMessagesService(ChatMessagesRepository chatMessageRepository, ChatMessagesMapper chatMessageMapper) {
        this.chatMessageRepository = chatMessageRepository;
        this.chatMessageMapper = chatMessageMapper;
    }

    public List<ChatMessagesDTO> findAllChatMessage(Long id) {
        return chatMessageRepository.findAllByChatDiscussionId(id).
            stream().map(chatMessageMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    public ChatMessagesDTO save(ChatMessagesDTO chatMessageDTO) {
        ChatMessage chatMessage = chatMessageMapper.toEntity(chatMessageDTO);
        chatMessage = chatMessageRepository.save(chatMessage);
        return chatMessageMapper.toDto(chatMessage);
    }

    public void delete(Long id) {
        chatMessageRepository.deleteById(id);
    }
}
