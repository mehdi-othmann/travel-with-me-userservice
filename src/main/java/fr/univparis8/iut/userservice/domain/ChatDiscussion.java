package fr.univparis8.iut.userservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ChatDiscussion.
 */
@Entity
@Table(name = "chat_discussion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ChatDiscussion extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "trip")
    private String trip;

    @OneToMany(mappedBy = "chatDiscussion")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ChatMessage> chatmessages = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public ChatDiscussion title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrip() {
        return trip;
    }

    public ChatDiscussion trip(String trip) {
        this.trip = trip;
        return this;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    public Set<ChatMessage> getChatmessages() {
        return chatmessages;
    }

    public ChatDiscussion chatmessages(Set<ChatMessage> chatMessages) {
        this.chatmessages = chatMessages;
        return this;
    }

    public ChatDiscussion addChatmessage(ChatMessage chatMessage) {
        this.chatmessages.add(chatMessage);
        chatMessage.setChatDiscussion(this);
        return this;
    }

    public ChatDiscussion removeChatmessage(ChatMessage chatMessage) {
        this.chatmessages.remove(chatMessage);
        chatMessage.setChatDiscussion(null);
        return this;
    }

    public void setChatmessages(Set<ChatMessage> chatMessages) {
        this.chatmessages = chatMessages;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChatDiscussion chatDiscussion = (ChatDiscussion) o;
        if (chatDiscussion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), chatDiscussion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ChatDiscussion{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", trip='" + getTrip() + "'" +
            "}";
    }
}
