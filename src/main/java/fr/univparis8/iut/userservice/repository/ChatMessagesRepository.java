package fr.univparis8.iut.userservice.repository;

import fr.univparis8.iut.userservice.domain.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ChatMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChatMessagesRepository extends JpaRepository<ChatMessage, Long> {

    List<ChatMessage> findAllByChatDiscussionId(long id);
}
