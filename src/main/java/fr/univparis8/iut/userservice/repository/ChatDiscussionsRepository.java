package fr.univparis8.iut.userservice.repository;

import fr.univparis8.iut.userservice.domain.ChatDiscussion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ChatDiscussion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChatDiscussionsRepository extends JpaRepository<ChatDiscussion, Long> {

    List <ChatDiscussion> findAllByTripEquals(String trip);
}
