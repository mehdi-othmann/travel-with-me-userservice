package fr.univparis8.iut.userservice.repository;

import fr.univparis8.iut.userservice.domain.ChatDiscussion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ChatDiscussion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChatDiscussionRepository extends JpaRepository<ChatDiscussion, Long> {

}
