package fr.univparis8.iut.userservice.web.rest;

import fr.univparis8.iut.userservice.service.ChatDiscussionsService;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionsDTO;
import fr.univparis8.iut.userservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.userservice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChatDiscussion.
 */
@RestController
@RequestMapping("/api")
public class ChatDiscussionsResource {

    private final ChatDiscussionsService chatDiscussionService;

    public ChatDiscussionsResource(ChatDiscussionsService chatDiscussionService) {
        this.chatDiscussionService = chatDiscussionService;
    }

    @GetMapping("/chat-discussions/trip/{id}")
    public List<ChatDiscussionsDTO> getAllChatDiscussions(@PathVariable String id) {
        return chatDiscussionService.findAllByTrip(id);
    }

    @GetMapping("/chat-discussion/{id}")
    public ResponseEntity<ChatDiscussionsDTO> getChatDiscussion(@PathVariable Long id) {
        Optional<ChatDiscussionsDTO> chatDiscussionDTO = chatDiscussionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chatDiscussionDTO);
    }

    @DeleteMapping("/chat-discussion/{id}")
    public ResponseEntity<Void> deleteChatDiscussion(@PathVariable Long id) {
        chatDiscussionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("discusssion", id.toString())).build();
    }

    @PutMapping("/chat-discussion")
    public ResponseEntity<ChatDiscussionsDTO> updateChatDiscussion(@RequestBody ChatDiscussionsDTO chatDiscussionDTO) throws URISyntaxException {
        if (chatDiscussionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", "discusssion", "idnull");
        }
        ChatDiscussionsDTO result = chatDiscussionService.save(chatDiscussionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("discusssion", chatDiscussionDTO.getId().toString()))
            .body(result);
    }

    @PostMapping("/chat-discussion")
    public ResponseEntity<ChatDiscussionsDTO> createChatDiscussion(@RequestBody ChatDiscussionsDTO chatDiscussionDTO) throws URISyntaxException {
        if (chatDiscussionDTO.getId() != null) {
            throw new BadRequestAlertException("A new chatDiscussion cannot already have an ID", "discusssion", "idexists");
        }
        ChatDiscussionsDTO result = chatDiscussionService.save(chatDiscussionDTO);
        return ResponseEntity.created(new URI("/api/chat-discussions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("discusssion", result.getId().toString()))
            .body(result);
    }

}
