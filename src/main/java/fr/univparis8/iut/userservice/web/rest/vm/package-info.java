/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.univparis8.iut.userservice.web.rest.vm;
