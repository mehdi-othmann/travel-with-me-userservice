package fr.univparis8.iut.userservice.web.rest;

import fr.univparis8.iut.userservice.service.GeneratorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GeneratorRessource {

    private final GeneratorService generatorService;

    public GeneratorRessource(GeneratorService generatorService) {
        this.generatorService = generatorService;
    }

    @GetMapping("/generate/chatDiscussion/{amount}")
    public void GenerateChatDiscussion(@PathVariable int amount) {
        generatorService.generateChatDiscussion(amount);
    }

    @GetMapping("/generate/offer/{amount}")
    public void GenerateOffer(@PathVariable int amount) {
        generatorService.generateOffer(amount);
    }

}
