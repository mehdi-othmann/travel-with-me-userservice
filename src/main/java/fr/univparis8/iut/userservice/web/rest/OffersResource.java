package fr.univparis8.iut.userservice.web.rest;

import fr.univparis8.iut.userservice.service.OfferService;
import fr.univparis8.iut.userservice.service.OffersService;
import fr.univparis8.iut.userservice.service.dto.OfferDTO;
import fr.univparis8.iut.userservice.service.dto.OffersDTO;
import fr.univparis8.iut.userservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.userservice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Offer.
 */
@RestController
@RequestMapping("/api")
public class OffersResource {

    private final Logger log = LoggerFactory.getLogger(OffersResource.class);

    private final OffersService offersService;

    public OffersResource(OffersService offersService) {
        this.offersService = offersService;
    }

    @GetMapping("/offres")
    public List<OffersDTO> getAllOffers() {
        log.debug("REST request to get all Offers");
        return offersService.findAll();
    }
}
