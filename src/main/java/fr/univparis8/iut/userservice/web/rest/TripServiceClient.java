package fr.univparis8.iut.userservice.web.rest;

import fr.univparis8.iut.userservice.service.dto.tripservice.TripsDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("travelwithmetripservice")
public interface TripServiceClient {

    @RequestMapping(method = RequestMethod.GET, value = "/api/voyages")
    List<TripsDTO> getAllVoyages();
}
