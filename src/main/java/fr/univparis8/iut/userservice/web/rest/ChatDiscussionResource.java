package fr.univparis8.iut.userservice.web.rest;
import fr.univparis8.iut.userservice.service.ChatDiscussionService;
import fr.univparis8.iut.userservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.userservice.web.rest.util.HeaderUtil;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChatDiscussion.
 */
@RestController
@RequestMapping("/api")
public class ChatDiscussionResource {

    private final Logger log = LoggerFactory.getLogger(ChatDiscussionResource.class);

    private static final String ENTITY_NAME = "travelWithMeUserServiceChatDiscussion";

    private final ChatDiscussionService chatDiscussionService;

    public ChatDiscussionResource(ChatDiscussionService chatDiscussionService) {
        this.chatDiscussionService = chatDiscussionService;
    }

    /**
     * POST  /chat-discussions : Create a new chatDiscussion.
     *
     * @param chatDiscussionDTO the chatDiscussionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chatDiscussionDTO, or with status 400 (Bad Request) if the chatDiscussion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chat-discussions")
    public ResponseEntity<ChatDiscussionDTO> createChatDiscussion(@RequestBody ChatDiscussionDTO chatDiscussionDTO) throws URISyntaxException {
        log.debug("REST request to save ChatDiscussion : {}", chatDiscussionDTO);
        if (chatDiscussionDTO.getId() != null) {
            throw new BadRequestAlertException("A new chatDiscussion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChatDiscussionDTO result = chatDiscussionService.save(chatDiscussionDTO);
        return ResponseEntity.created(new URI("/api/chat-discussions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chat-discussions : Updates an existing chatDiscussion.
     *
     * @param chatDiscussionDTO the chatDiscussionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chatDiscussionDTO,
     * or with status 400 (Bad Request) if the chatDiscussionDTO is not valid,
     * or with status 500 (Internal Server Error) if the chatDiscussionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chat-discussions")
    public ResponseEntity<ChatDiscussionDTO> updateChatDiscussion(@RequestBody ChatDiscussionDTO chatDiscussionDTO) throws URISyntaxException {
        log.debug("REST request to update ChatDiscussion : {}", chatDiscussionDTO);
        if (chatDiscussionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChatDiscussionDTO result = chatDiscussionService.save(chatDiscussionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chatDiscussionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chat-discussions : get all the chatDiscussions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chatDiscussions in body
     */
    @GetMapping("/chat-discussions")
    public List<ChatDiscussionDTO> getAllChatDiscussions() {
        log.debug("REST request to get all ChatDiscussions");
        return chatDiscussionService.findAll();
    }

    /**
     * GET  /chat-discussions/:id : get the "id" chatDiscussion.
     *
     * @param id the id of the chatDiscussionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chatDiscussionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/chat-discussions/{id}")
    public ResponseEntity<ChatDiscussionDTO> getChatDiscussion(@PathVariable Long id) {
        log.debug("REST request to get ChatDiscussion : {}", id);
        Optional<ChatDiscussionDTO> chatDiscussionDTO = chatDiscussionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chatDiscussionDTO);
    }

    /**
     * DELETE  /chat-discussions/:id : delete the "id" chatDiscussion.
     *
     * @param id the id of the chatDiscussionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chat-discussions/{id}")
    public ResponseEntity<Void> deleteChatDiscussion(@PathVariable Long id) {
        log.debug("REST request to delete ChatDiscussion : {}", id);
        chatDiscussionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
