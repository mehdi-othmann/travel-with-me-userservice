package fr.univparis8.iut.userservice.web.rest;

import fr.univparis8.iut.userservice.service.ChatMessagesService;
import fr.univparis8.iut.userservice.service.dto.ChatMessageDTO;
import fr.univparis8.iut.userservice.service.dto.ChatMessagesDTO;
import fr.univparis8.iut.userservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.userservice.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * REST controller for managing ChatMessage.
 */
@RestController
@RequestMapping("/api")
public class ChatMessagesResource {

    private final Logger log = LoggerFactory.getLogger(ChatMessagesResource.class);

    private static final String ENTITY_NAME = "travelWithMeUserServiceChatMessage";

    private final ChatMessagesService chatMessageService;

    public ChatMessagesResource(ChatMessagesService chatMessageService) {
        this.chatMessageService = chatMessageService;
    }

    @GetMapping("/chat-message/chat-discussion/{id}")
    public List<ChatMessagesDTO> getChatDiscussionChatMessage(@PathVariable Long id) {
        return chatMessageService.findAllChatMessage(id);
    }

    @PostMapping("/chat-message")
    public ResponseEntity<ChatMessagesDTO> createChatMessage(@RequestBody ChatMessagesDTO chatMessageDTO) throws URISyntaxException {
        if (chatMessageDTO.getId() != null) {
            throw new BadRequestAlertException("A new chatMessage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChatMessagesDTO result = chatMessageService.save(chatMessageDTO);
        return ResponseEntity.created(new URI("/api/chat-messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @DeleteMapping("/chat-message/{id}")
    public ResponseEntity<Void> deleteChatMessage(@PathVariable Long id) {
        chatMessageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PutMapping("/chat-message")
    public ResponseEntity<ChatMessagesDTO> updateChatMessage(@RequestBody ChatMessagesDTO chatMessageDTO) throws URISyntaxException {
        if (chatMessageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ChatMessagesDTO result = chatMessageService.save(chatMessageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chatMessageDTO.getId().toString()))
            .body(result);
    }

}
