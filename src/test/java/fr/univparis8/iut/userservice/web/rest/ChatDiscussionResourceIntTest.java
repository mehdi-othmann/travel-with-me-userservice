package fr.univparis8.iut.userservice.web.rest;

import fr.univparis8.iut.userservice.TravelWithMeUserServiceApp;

import fr.univparis8.iut.userservice.domain.ChatDiscussion;
import fr.univparis8.iut.userservice.repository.ChatDiscussionRepository;
import fr.univparis8.iut.userservice.service.ChatDiscussionService;
import fr.univparis8.iut.userservice.service.dto.ChatDiscussionDTO;
import fr.univparis8.iut.userservice.service.mapper.ChatDiscussionMapper;
import fr.univparis8.iut.userservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static fr.univparis8.iut.userservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChatDiscussionResource REST controller.
 *
 * @see ChatDiscussionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TravelWithMeUserServiceApp.class)
public class ChatDiscussionResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_TRIP = "AAAAAAAAAA";
    private static final String UPDATED_TRIP = "BBBBBBBBBB";

    @Autowired
    private ChatDiscussionRepository chatDiscussionRepository;

    @Autowired
    private ChatDiscussionMapper chatDiscussionMapper;

    @Autowired
    private ChatDiscussionService chatDiscussionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restChatDiscussionMockMvc;

    private ChatDiscussion chatDiscussion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ChatDiscussionResource chatDiscussionResource = new ChatDiscussionResource(chatDiscussionService);
        this.restChatDiscussionMockMvc = MockMvcBuilders.standaloneSetup(chatDiscussionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChatDiscussion createEntity(EntityManager em) {
        ChatDiscussion chatDiscussion = new ChatDiscussion()
            .title(DEFAULT_TITLE)
            .trip(DEFAULT_TRIP);
        return chatDiscussion;
    }

    @Before
    public void initTest() {
        chatDiscussion = createEntity(em);
    }

    @Test
    @Transactional
    public void createChatDiscussion() throws Exception {
        int databaseSizeBeforeCreate = chatDiscussionRepository.findAll().size();

        // Create the ChatDiscussion
        ChatDiscussionDTO chatDiscussionDTO = chatDiscussionMapper.toDto(chatDiscussion);
        restChatDiscussionMockMvc.perform(post("/api/chat-discussions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatDiscussionDTO)))
            .andExpect(status().isCreated());

        // Validate the ChatDiscussion in the database
        List<ChatDiscussion> chatDiscussionList = chatDiscussionRepository.findAll();
        assertThat(chatDiscussionList).hasSize(databaseSizeBeforeCreate + 1);
        ChatDiscussion testChatDiscussion = chatDiscussionList.get(chatDiscussionList.size() - 1);
        assertThat(testChatDiscussion.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testChatDiscussion.getTrip()).isEqualTo(DEFAULT_TRIP);
    }

    @Test
    @Transactional
    public void createChatDiscussionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chatDiscussionRepository.findAll().size();

        // Create the ChatDiscussion with an existing ID
        chatDiscussion.setId(1L);
        ChatDiscussionDTO chatDiscussionDTO = chatDiscussionMapper.toDto(chatDiscussion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChatDiscussionMockMvc.perform(post("/api/chat-discussions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatDiscussionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChatDiscussion in the database
        List<ChatDiscussion> chatDiscussionList = chatDiscussionRepository.findAll();
        assertThat(chatDiscussionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChatDiscussions() throws Exception {
        // Initialize the database
        chatDiscussionRepository.saveAndFlush(chatDiscussion);

        // Get all the chatDiscussionList
        restChatDiscussionMockMvc.perform(get("/api/chat-discussions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chatDiscussion.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].trip").value(hasItem(DEFAULT_TRIP.toString())));
    }
    
    @Test
    @Transactional
    public void getChatDiscussion() throws Exception {
        // Initialize the database
        chatDiscussionRepository.saveAndFlush(chatDiscussion);

        // Get the chatDiscussion
        restChatDiscussionMockMvc.perform(get("/api/chat-discussions/{id}", chatDiscussion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chatDiscussion.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.trip").value(DEFAULT_TRIP.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingChatDiscussion() throws Exception {
        // Get the chatDiscussion
        restChatDiscussionMockMvc.perform(get("/api/chat-discussions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChatDiscussion() throws Exception {
        // Initialize the database
        chatDiscussionRepository.saveAndFlush(chatDiscussion);

        int databaseSizeBeforeUpdate = chatDiscussionRepository.findAll().size();

        // Update the chatDiscussion
        ChatDiscussion updatedChatDiscussion = chatDiscussionRepository.findById(chatDiscussion.getId()).get();
        // Disconnect from session so that the updates on updatedChatDiscussion are not directly saved in db
        em.detach(updatedChatDiscussion);
        updatedChatDiscussion
            .title(UPDATED_TITLE)
            .trip(UPDATED_TRIP);
        ChatDiscussionDTO chatDiscussionDTO = chatDiscussionMapper.toDto(updatedChatDiscussion);

        restChatDiscussionMockMvc.perform(put("/api/chat-discussions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatDiscussionDTO)))
            .andExpect(status().isOk());

        // Validate the ChatDiscussion in the database
        List<ChatDiscussion> chatDiscussionList = chatDiscussionRepository.findAll();
        assertThat(chatDiscussionList).hasSize(databaseSizeBeforeUpdate);
        ChatDiscussion testChatDiscussion = chatDiscussionList.get(chatDiscussionList.size() - 1);
        assertThat(testChatDiscussion.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testChatDiscussion.getTrip()).isEqualTo(UPDATED_TRIP);
    }

    @Test
    @Transactional
    public void updateNonExistingChatDiscussion() throws Exception {
        int databaseSizeBeforeUpdate = chatDiscussionRepository.findAll().size();

        // Create the ChatDiscussion
        ChatDiscussionDTO chatDiscussionDTO = chatDiscussionMapper.toDto(chatDiscussion);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChatDiscussionMockMvc.perform(put("/api/chat-discussions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatDiscussionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ChatDiscussion in the database
        List<ChatDiscussion> chatDiscussionList = chatDiscussionRepository.findAll();
        assertThat(chatDiscussionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteChatDiscussion() throws Exception {
        // Initialize the database
        chatDiscussionRepository.saveAndFlush(chatDiscussion);

        int databaseSizeBeforeDelete = chatDiscussionRepository.findAll().size();

        // Delete the chatDiscussion
        restChatDiscussionMockMvc.perform(delete("/api/chat-discussions/{id}", chatDiscussion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChatDiscussion> chatDiscussionList = chatDiscussionRepository.findAll();
        assertThat(chatDiscussionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChatDiscussion.class);
        ChatDiscussion chatDiscussion1 = new ChatDiscussion();
        chatDiscussion1.setId(1L);
        ChatDiscussion chatDiscussion2 = new ChatDiscussion();
        chatDiscussion2.setId(chatDiscussion1.getId());
        assertThat(chatDiscussion1).isEqualTo(chatDiscussion2);
        chatDiscussion2.setId(2L);
        assertThat(chatDiscussion1).isNotEqualTo(chatDiscussion2);
        chatDiscussion1.setId(null);
        assertThat(chatDiscussion1).isNotEqualTo(chatDiscussion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChatDiscussionDTO.class);
        ChatDiscussionDTO chatDiscussionDTO1 = new ChatDiscussionDTO();
        chatDiscussionDTO1.setId(1L);
        ChatDiscussionDTO chatDiscussionDTO2 = new ChatDiscussionDTO();
        assertThat(chatDiscussionDTO1).isNotEqualTo(chatDiscussionDTO2);
        chatDiscussionDTO2.setId(chatDiscussionDTO1.getId());
        assertThat(chatDiscussionDTO1).isEqualTo(chatDiscussionDTO2);
        chatDiscussionDTO2.setId(2L);
        assertThat(chatDiscussionDTO1).isNotEqualTo(chatDiscussionDTO2);
        chatDiscussionDTO1.setId(null);
        assertThat(chatDiscussionDTO1).isNotEqualTo(chatDiscussionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(chatDiscussionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(chatDiscussionMapper.fromId(null)).isNull();
    }
}
